# Movie Clip Trailers

### Live [Movie Clips](https://movie-clips-app.netlify.com)

![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Fmovie-clips-app-react.png?alt=media&token=34d67230-ae9e-4779-9def-42ba6d828063)
![alt text](https://firebasestorage.googleapis.com/v0/b/fir-web-app-40bca.appspot.com/o/photos%2Fmovie-clips-react-video.png?alt=media&token=dc6bdf72-855f-4fe0-bb21-6425474f6b51)

### Description

"Movie Clip Trailers" is an application to view the 20 latest, highest rated and themost popular movies. In addition, the user can search detail information for his favorite movies and ,waht is more, watch the trailer. Each user can add the selected movie to their favorites clicking by on the button "Add to watchlist".

### Project structure and implementation of the project have been implemented and designed by Karol Kozer.

### The project was made (for personal purposes and portfolio) based on graphic project Movie Aplication UI Dribble

### The project includes:

React , ES6, Babel , Sass , Themoviedb API

### Themoviedb API

The Movie Database (TMDb) is a community built movie and TV database. Every piece of data has been added by our amazing community dating back to 2008. TMDb's strong international focus and breadth of data is largely unmatched and something we're incredibly proud of. Put simply, we live and breathe community and that's precisely what makes us different.

[Themoviedb](https://www.themoviedb.org/)

### Run project

Require [Node](https://nodejs.org/en/)

Install the dependencies:

```
npm install
```

Run dev server:

```
npm run start
```

Localhost:
`http://localhost:3000/`
