import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
// import registerServiceWorker from './registerServiceWorker';

import './css/main.css';

import App from './components/App';

const Root = () => {
     return (
          <BrowserRouter>
               <App/>
          </BrowserRouter>
     )
}



ReactDOM.render(<Root />, document.getElementById('root'));
// registerServiceWorker();
