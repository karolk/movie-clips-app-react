// const Header = new Headers({
//      'Access-Control-Allow-Origin': '*',
//      'mode':'no-core'
// });

const status = (response) => {
     if(response.status >= 200 && response.status < 400) {
          return Promise.resolve(response);
     } else {
          return Promise.reject({
               status: response.status,
               statusText: response.statusText
          })
     }
}

const fetchData = (url) => {
     return fetch(url)
               .then(status)
               .then(data => data.json())
               .catch(error => alert(Error(`Request filed HTTP status is ${error.status} ${error.statusText}`)));
}

export const fetchGenres = (category) => {
     const url = `https://api.themoviedb.org/3/genre/movie/list?api_key=4aa3a7b345e94044e381eb3b661f136c`;

     return fetchData(url);
};

export const fetchMovies = (category) => {
     const url = `https://api.themoviedb.org/3/movie/${category}?api_key=4aa3a7b345e94044e381eb3b661f136c`;

     return fetchData(url);
};
export const fetchSearchMovies = (query) => {
     const url = `https://api.themoviedb.org/3/search/movie?api_key=4aa3a7b345e94044e381eb3b661f136c&query=${query}&include_adult=false`;

     return fetchData(url);
};

export const fetchMovieDetails = (id) => {
     const url = `https://api.themoviedb.org/3/movie/${id}?api_key=4aa3a7b345e94044e381eb3b661f136c&append_to_response=videos,images,credits`;

     return fetchData(url);
};



