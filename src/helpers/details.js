export const runtime = (time) => {
     let [ hours, min ] = (time / 60).toFixed(2).split('.');
     hours = parseInt(hours, 0);
     min = parseFloat(min);
     if(min > 60) {
          hours += 1;
          min -= 60;
     }
     return `${hours}h ${min}m`;
};

export const findDirector = crew => crew.length > 0 ? crew.find(job => job.job === 'Director').name : null;

export const checkBackdrop = (backdrop,defaultBackdrop) => backdrop !== null ? `https://image.tmdb.org/t/p/w1280${backdrop}` : defaultBackdrop;

export const checkPoster = (poster, defaultPoster) => poster !== null ? `https://image.tmdb.org/t/p/w342/${poster}` : defaultPoster;

export const chackVoteAverage = vote => vote === 0 ? 'details__vote-average details__vote-average--hidden' : 'details__vote-average';