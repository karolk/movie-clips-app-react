const headingText = {
     now_playing: 'New Releases',
     top_rated: 'Trending',
     popular: 'Popular',
     watch_later: 'Watch Later'

}

const clearPath = (element) => element.replace(/\//g, '');

export const findHeading = (element) => {
     const text = clearPath(element);
     return headingText[text];
}

export const transformText = (element) => {
     return clearPath(element);
}

