import React from 'react';
import { Link } from 'react-router-dom';
import posterDefault from '../images/default_img/themoviedb-poster.png';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import NotFound from './Not_Found'; 

export default class WatchLater extends React.Component {

     state = {
          watchList: []
     }

     componentWillMount() {
         // check localStorage
          const localStorageRef = localStorage.getItem('watchlist');
          if(localStorageRef) {
               this.setState({ watchList: JSON.parse(localStorageRef) });
          }
          
     }

     poster = (path) => !path ? posterDefault : `https://image.tmdb.org/t/p/w342/${path}`;

     movie = (key, details, index) => {
        const duration = (index + 1) * 600;
          return (
              <CSSTransition key={details.id} in classNames="fadeInLeft" appear timeout={duration}>
               <li key={key} className="movie">
                    <Link to={`/movie/details/${details.id}`} className="movie__link">
                         <span className={details.vote_average !== "0" ? 'movie__rate' : 'movie__rate--hidden'}>{details.vote_average}</span>
                         <img src={this.poster(details.poster_path)} alt="Movie Poster" className="movie__poster"/>
                    </Link>
                    <h3 className="movie__title">{details.title}</h3>
               </li>
               </CSSTransition>
          )
     }

     render() {
          if(this.state.watchList.length === 0) return <NotFound/>;
          return(
               <section className="section section__watch-later">
                    <div className="movies">
                    <h1 className="heading-first">Watch Later</h1>
                        <TransitionGroup component="ul" className="movies__list">
                              {
                                   this.state.watchList.map((movie, index) => this.movie(movie.id, movie, index))
                              }
                        </TransitionGroup>
                    </div>
               </section>
          )
     }
}