import React from 'react';

const NotFound = () => {
        return (
            <div className="not_found">
                <h2 className="not_found__message">There are no movies that matched your query</h2>
            </div> 
        );
}

export default NotFound;
