import React from 'react';
import { transformText, findHeading }  from '../helpers/transform_text';
import { fetchMovies } from '../helpers/fetch_data';
import PropTypes from 'prop-types';

import Movies from './Movies';
import Loading from './Loading';

export default class Popular extends React.Component {

     static contextTypes = {
          location: PropTypes.object
     }

     state = {
          isLoaded: false,
          movies: []
     }


     componentWillMount() {
          // Get the path
          const path = this.props.location.pathname;
          this.setState({ text: findHeading(path) });

          const category = transformText(path);
          // Fetch movies
          fetchMovies(category)
               .then(data => this.setState({ 
                    movies: data.results,
                    isLoaded: true 
               }));
     }

     render() {
          if(!this.state.isLoaded) return <Loading/>;
          return(
               <section className="section section__popular">
                    <Movies movies={this.state.movies} text={this.state.text}/>
               </section>
          )
     }
}