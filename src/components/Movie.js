import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import posterDefault from '../images/default_img/themoviedb-poster.png';
import { CSSTransition } from 'react-transition-group';  

const Movie = ({genres, details, index}) => {

    const movieGenres = genres.map(genre => {
        // Check if genre is undefined
        if(genre === undefined) return '';
        return(
            <span key={genre.id} className="movie__genre">{genre.name}</span>
        );
    });

    const poster = (path) => path !== null ? `https://image.tmdb.org/t/p/w342/${path}`: posterDefault;
    
    const duration = (index + 1) * 600;
    
    return(
        <CSSTransition in classNames="fadeInLeft" appear timeout={duration}>
            <li className="movie">
                <Link to={`/movie/details/${details.id}`} className="movie__link">
                    <span className={details.vote_average !== 0 ? 'movie__rate' : 'movie__rate--hidden'}>{details.vote_average}</span>
                    <img src={poster(details.poster_path)} alt="Movie Poster" className="movie__poster"/>
                </Link>
                <h3 className="movie__title">{details.title}</h3>
                <div className="movie__genres">
                    { movieGenres }
                </div>
            </li>
         </CSSTransition>
    );
}

Movie.contextTypes = {
    genres: PropTypes.array,
    details: PropTypes.object
}

export default Movie;
