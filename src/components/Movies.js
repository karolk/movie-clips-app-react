import React from 'react';
import Movie from './Movie';
import { fetchGenres } from '../helpers/fetch_data';
import PropTypes from 'prop-types';
import { TransitionGroup } from 'react-transition-group'; 

import NotFound from './Not_Found';
import Loading from './Loading';

export default class Movies extends React.Component {

    static contextTypes = {
        movies: PropTypes.array
    }

    state = {
        isLoaded: false, 
        genres: []
    }

    componentWillMount() {
        // Fetch movie genres
        fetchGenres().then(genresData => this.setState({ 
            genres: genresData.genres,
            isLoaded: true 
        }));
    }

    findGenres = () => {
        const genres = this.state.genres;
        // Get movie genres ids array
        const movieGenreIDs = this.props.movies.map(el => el.genre_ids);
        // Find movie genre names
        return movieGenreIDs.map(ids => ids.map(id => genres.find(genre => genre.id === id)));
    }

    render() {
        const matchedGenres = this.findGenres();
        if(!this.state.isLoaded) return <Loading/>;
        if (this.props.movies.length === 0) return <NotFound/>;

        return(
            <div className="movies">
                <h1 className="heading-first">{this.props.text}</h1>
                    <TransitionGroup component="ul" className="movies__list">
                    {
                        this.props.movies.map((movie, index) => 
                        <Movie key={movie.id} details={movie} genres={matchedGenres[index]} index={index}/>)
                    }
                    </TransitionGroup>
            </div>
        )
    }
}
