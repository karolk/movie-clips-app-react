import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'

import Header from './Header';
import Sidebar from './Sidebar';
import NowPlaying from './Now_Playing';
import TopRated from './Top_Rated';
import Popular from './Popular';
import WatchLater from './Watch_Later';
import NotFound from './Not_Found';
import SearchMovies from './Search_Movies';
import Details from './Details';

class App extends React.Component {

     state = {
          userQuery: null,
          isMobileMenuOpen: false
     }

     getUserQuery = (query) => {
          this.setState({ userQuery: query })
     };

     openMobileMenu = () => this.setState({isMobileMenuOpen: !this.state.isMobileMenuOpen})

     render() {
          return (
               <div className="container">
                    <Header openMobileMenu={this.openMobileMenu} getUserQuery={this.getUserQuery}/>
                    <div className="content">
                         <Sidebar openMobileMenu={this.openMobileMenu} isMenuOpen={this.state.isMobileMenuOpen}/>
                         <main className="main">
                              <Switch>
                                   <Redirect exact from="/" to="/now_playing" />
                                   <Route path="/now_playing" component={NowPlaying}/>
                                   <Route path="/top_rated" component={TopRated}/>
                                   <Route path="/popular" component={Popular}/>
                                   <Route path="/watch_later" component={WatchLater}/>
                                   <Route path="/search/movies/:query" 
                                   component={() => 
                                        <SearchMovies query={this.state.userQuery}/>
                                   }
                                   />
                                   <Route path="/movie/details/:id" component={Details}/>
                                   <Route component={NotFound}/>
                              </Switch>
                         </main>
                    </div>
               </div>
          );
     }
}

export default App;
