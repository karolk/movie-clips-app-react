import React from 'react';
import { NavLink } from 'react-router-dom';
import icon from '../images/svg/sprite-icon.svg';

const Nav = (props) => {

     const openMenu = () => props.openMobileMenu();

     return(
          <nav className="nav">
               <ul className="nav__list">
                    <li className="nav__item">
                         <NavLink 
                         exact activeClassName="nav__link--active" 
                         to="/now_playing" className="nav__link"
                         onClick={openMenu}>
                              New Releases
                              <svg className="nav__arrow icon icon--arrow">
                                   <use xlinkHref={`${icon}#icon-keyboard_backspaceicon-`}></use>
                              </svg>
                         </NavLink>
                    </li>
                    <li className="nav__item">
                         <NavLink 
                         activeClassName="nav__link--active" 
                         to="/top_rated" className="nav__link"
                         onClick={openMenu}>
                              Trending
                              <svg className="nav__arrow icon icon--arrow">
                                   <use xlinkHref={`${icon}#icon-keyboard_backspaceicon-`}></use>
                              </svg>
                         </NavLink>
                    </li>
                    <li className="nav__item">
                         <NavLink 
                         activeClassName="nav__link--active" 
                         to="/popular" className="nav__link"
                         onClick={openMenu}>
                              Popular
                              <svg className="nav__arrow icon icon--arrow">
                                   <use xlinkHref={`${icon}#icon-keyboard_backspaceicon-`}></use>
                              </svg>
                         </NavLink>
                    </li>
                    <li className="nav__item">
                         <NavLink 
                         activeClassName="nav__link--active" 
                         to="/watch_later" className="nav__link"
                         onClick={openMenu}>
                              Watch Later
                              <svg className="nav__arrow icon icon--arrow">
                                   <use xlinkHref={`${icon}#icon-keyboard_backspaceicon-`}></use>
                              </svg>
                         </NavLink>
                    </li>
                    <li className="nav__item nav__item--sing-out">
                         <button className="btn btn--sign-out  btn--mobile-sign-out">Sign out</button>
                    </li>
               </ul>
               <span className="nav__line">&nbsp;</span>
          </nav>
     );
}

export default Nav;