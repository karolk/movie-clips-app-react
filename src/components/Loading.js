import React from 'react';

const Loading = () => {
     return (
          <div className="loading">
               <div className="loading__animation">
                    <span className="loading__circle"></span>
               </div>
               <h3 className="loading__description">Loading...</h3>
          </div>
     );
};

export default Loading;