import React from 'react';
import { fetchMovieDetails } from '../helpers/fetch_data';
import { runtime, findDirector, checkBackdrop, checkPoster , chackVoteAverage } from '../helpers/details';
import PropTypes from 'prop-types';
import icon from '../images/svg/sprite-icon.svg';
import defaultBackdrop from '../images/default_img/backdrop.png';
import defaultPoster from '../images/default_img/themoviedb-poster.png';

import Loading from './Loading';
import Trailer from './Trailer';

import { CSSTransition } from 'react-transition-group'

export default class Details extends React.Component {

    static contextTypes = {
        match: PropTypes.object
    }

     state = {
          isLoaded: false,
          play: false,
          trailerKey: null,
          details: [],
          watchList: []
     }

     componentWillMount() {
         // Get movie id
        const id = this.props.match.params.id;
        // Fetch selected movie
        fetchMovieDetails(id)
            .then(data => this.setState({
                isLoaded: true,
                details: data
        }));
        // Check the localStorage
        const localStorageRef = localStorage.getItem('watchlist');
        if(localStorageRef) this.setState({ watchList: JSON.parse(localStorageRef) });
          
     }
     
     componentWillUpdate(nextProps, nextState) {
        // Pass watchlist to localstorage
        localStorage.setItem('watchlist', JSON.stringify(nextState.watchList));
     }
     // Go back to previous page
     closeDetails = () => this.props.history.goBack();

     playTrailer = () => {
        // Get trailer yt key
        const trailerKey = this.refs.trailer.dataset.key;
        // Update state
        this.setState({
            play: true,
            trailerKey
        });
     };

     addToWatchList = () => {
         const watchList = [...this.state.watchList];
         // Set new movie to watch list
         const movie = {
             id: this.refs.watchlist.dataset.id,
             title: this.refs.watchlist.dataset.title,
             poster_path: this.refs.watchlist.dataset.poster_path,
             vote_average: this.refs.watchlist.dataset.vote_average
         }
         const checkWatchList = watchList.find(item => item.id === movie.id);
         // check is this movie is in the watchlist
         if(!checkWatchList) {
            watchList.push(movie);
            this.setState({ watchList });
         }
     }

     removeFromWatchList = () => {
        const watchList = [...this.state.watchList];
        // Get movie id
        const id = this.refs.id.dataset.id;
        // Find movie in the watchist
        const index = watchList.findIndex(movie => movie.id === id);
        // Remove movie
        watchList.splice(index, 1);
        // Update state
        this.setState({ watchList });
     }

     poster = (poster) => {
          return (
               <div className="details__poster">
                   <img className="details__poster-img" src={checkPoster(poster, defaultPoster)} alt="Poster"/>
               </div>
          )
     }

     video = (trailerKey) => {
         // if play set to true, hide icon and btn play trailer
         const isPlayed = this.state.play ? 'details__play--hidden' : 'details__play';
          return (
               <div className={isPlayed}>
                    <svg className="icon icon--play">
                         <use xlinkHref={`${icon}#icon-ondemand_videoicon-`}></use>
                    </svg>
                    <button onClick={this.playTrailer} ref="trailer" data-key={trailerKey} className="btn btn--play">Play Trailer</button>
               </div>
          )
     }

     findTrailer = (details) => {
        // if there is not a trailer video  return poster
        if(details.videos.results.length === 0) return this.poster(details.poster_path);
        // Find trailer  typein video 
        const trailerKey =  details.videos.results.find(video => video.type === 'Trailer' || video.type === 'Clip').key;
        // check if it is a trailer
        return trailerKey ? this.video(trailerKey) : this.poster(details.poster_path);
      }

      checkIsInWatchList = (details) => {
          // Find movie in watchlist
          const checkWatchList = this.state.watchList.find(item => parseInt(item.id, 0) === details.id);
          // if there  is not a movie in watch list return btn add
          if(!checkWatchList) {
              return (
                <CSSTransition in appear classNames="fadeUp"  timeout={800}>
                  <button onClick={this.addToWatchList} className="btn btn--add" 
                    ref="watchlist" 
                    data-id={details.id}
                    data-title={details.title} 
                    data-vote_average={details.vote_average}
                    data-poster_path={details.poster_path}>
                        <svg className="icon icon--add">
                                <use xlinkHref={`${icon}#icon-addicon-`}></use>
                        </svg>
                        Add To Watchlist
                    </button>
                </CSSTransition>
              )
          }
          // Return btn remove from watchlist
          return (
                <button 
                onClick={this.removeFromWatchList} 
                ref="id" 
                data-id={details.id} 
                className="btn btn--remove">
                    Remove From WatchList
                </button>
          )
      }

     movieDetails = () => {
          const details = this.state.details;
          return (
               <div className="details__overlay">
                    <img src=
                    { checkBackdrop(details.backdrop_path, defaultBackdrop) } 
                    alt="Backdrop Poster" className="details__backdrop"/>
                    <button onClick={this.closeDetails} className="btn btn--close">
                         <svg className="icon icon--close">
                              <use xlinkHref={`${icon}#icon-closeicon-`}></use>
                         </svg>
                    </button>
                    <div className="details__content">
                         { this.findTrailer(details) }
                         <Trailer play={this.state.play} trailerKey={this.state.trailerKey}/>
                         <div className="details__reference">
                              <h2 className="heading-second">{details.title}</h2>
                              <div className="details__movie-info">
                                        <span className={chackVoteAverage(details.vote_average)}>
                                             <span className="details__higlight">{details.vote_average}
                                             </span>
                                             /10
                                        </span>
                                        <span className="details__year">
                                             { details.release_date.split('-').slice(0,1) }
                                        </span>
                                        <span className="details__runtime">
                                             { details.runtime > 0 ? runtime(details.runtime) : '' }
                                        </span>
                              </div>
                              <p className="details__overview">{ details.overview }</p>
                              <p className=
                                   {details.credits.crew.length > 0 ? 'details__director' : 'details__director--hidden' }>
                                   Directed By: {findDirector(details.credits.crew)}</p>
                              <div className="details__add-to-watchlist">
                                   { this.checkIsInWatchList(details) }
                              </div>
                         </div>
                    </div>
               </div>
          )
     }

     render() {

          return(
               <div className="details">
                    {
                         !this.state.isLoaded ? <Loading/> : this.movieDetails()
                    }
               </div>
          )
     }
}
