import React from 'react';

const Trailer = ({play, trailerKey}) => {

     if(play === false) return null;

     return (
          <div className="trailer">
               <iframe title={trailerKey}
                    className="trailer__video" width="480" height="315" src={`https://www.youtube.com/embed/${trailerKey}?rel=0&amp;autoplay=1;controls=0&amp;showinfo=0`}
                    frameBorder="0" allowFullScreen>
               </iframe>
          </div>
     )
}

export default Trailer;