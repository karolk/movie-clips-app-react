import React from 'react';
import { fetchSearchMovies } from '../helpers/fetch_data';
import PropTypes from 'prop-types';

import Movies from './Movies';
import NotFound from './Not_Found';
import Loading from './Loading';

export default class SearchMovies extends React.Component {

    static contextTypes = {
        router: PropTypes.object.isRequired,
        query: PropTypes.string
    }

     state = {
          isLoaded: false,
          movies: [],
          totalResults: 0
     }

     componentWillMount() {
         // Get userQuery
         let userQuery = this.props.query;
         // Check if query is null
          if(userQuery === null) {
               let path = this.context.router.route.location.pathname;
               userQuery = path.split(/\//g).slice(3).join();
          }

          fetchSearchMovies(userQuery)
            .then(data => this.setState({
                movies: data.results,
                totalResults: data.total_results,
                isLoaded: true
            }));
     }
     

     render() {
          if(!this.state.isLoaded) return <Loading/>;
          else if (this.state.totalResults === 0) return <NotFound/>;

          return(
               <section className="section section__search">
                    <Movies movies={this.state.movies} text={"Results"} />
               </section>
          )
     }
}
