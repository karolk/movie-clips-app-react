import React from 'react';
import glass from '../images/svg/sprite-icon.svg';
import PropTypes from 'prop-types';

export default class SearchForm extends React.Component {

    static contextTypes = {
        router: PropTypes.object.isRequired
     }

    getInput = (e) => {
        e.preventDefault();
        // Get user query
        let query = this.refs.query.value;
        query = query.trim().replace(/\s/g, '%20');
        // Add query to App state
        this.props.getUserQuery(query);
        // Reset Form
        e.currentTarget.reset();
        // Change page to /search/movies/:query
        this.context.router.history.push(`/search/movies/${query}`);
    }

     render() {
          return (
               <form onSubmit={this.getInput} className="search_form">
                    <button className="btn btn--search">
                        <svg className="icon icon--glass">
                            <use xlinkHref={`${glass}#icon-magnifying-glassicon-`}></use>
                        </svg>
                    </button>
                    <input ref="query" type="text" className="search_form__input" placeholder="Search movies" required/>
               </form>
          )
     }
}
