import React from 'react';
import logo from '../images/svg/sprite-icon.svg';
import SearchForm from './Search_Form';

const Header = ({ getUserQuery, openMobileMenu }) => {

     const openMenu = () => openMobileMenu();

     return (
          <header className="header">
               <div className="header__logo-box">
                    <svg className="header__logo">
                         <use xlinkHref={`${logo}#icon-subscriptionsicon-`}></use>
                    </svg>
                    <button onClick={openMenu} className="btn btn--menu">
                         <svg className="header__logo header__logo--mobile">
                              <use xlinkHref={`${logo}#icon-subscriptionsicon-`}></use>
                         </svg>
                    </button>
               </div>
               <SearchForm getUserQuery={getUserQuery}/>
               <button className="btn btn--sign-out">Sign out</button>
          </header>
     )
}

export default Header;