import React from 'react';
import user from '../images/user/user-1.png';
import user4 from '../images/user/user-4.png';

const Activity = () => {
     return (
          <div className="activity">
               <h3 className="activity__heading">
                    Last Activity 
                    <span className="activity__number">2</span>
               </h3>

               <div className="activity__users">
                    <div className="activity__user">
                         <img src={user} className="activity__user-image" alt="User"/>
                         <p className="activity__message">
                              <span className="activity__text-highlight">Don Francis </span>
                              wants to be your friend
                         </p>
                    </div>
                    <div className="activity__user">
                         <img src={user4} className="activity__user-image" alt="User"/>
                         <p className="activity__message">
                              <span className="activity__text-highlight">Nettie Carr </span>
                              has recomended you: <span className="activity__text-highlight">The Witch</span>
                         </p>
                    </div>
               </div>
          </div>
     );
}
export default Activity;