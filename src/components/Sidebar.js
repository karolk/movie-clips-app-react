import React from 'react';
import userImage from '../images/user/user-2.png';
import themoviedbLogo from '../images/svg/themoviedb-logo-rectangle.svg';
import Nav from './Nav';
import Activity from './Activity';

export default class Sidebar extends React.Component {
     render() {
          const isOpen = this.props.isMenuOpen ? 'sidebar--open' : '';
          return(
               <div className={`sidebar ${isOpen}`}>
                    <div className="user">
                         <img src={userImage} alt="User" className="user__image"/>
                         <p className="user__name">Autin Bush</p>
                    </div>
                    <Nav openMobileMenu={this.props.openMobileMenu}/>
                    <Activity/>
                    <div className="sidebar__logo-box">
                         <img className="sidebar__logo" src={themoviedbLogo} alt="The moviedb Logo"/>
                    </div>
               </div>
          );
     }
}